<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:89:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\member\member_edit.html";i:1566444834;s:92:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\layout\edit_btn_group.html";i:1562986698;}*/ ?>
<form action="<?php echo url(); ?>" method="post" class="form_single">
    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>用户名</label>
              <span>（用户名，主要用于登录）</span>
              <input class="form-control" name="username" placeholder="请输入用户名" value="<?php echo $info['username']; ?>" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>昵称</label>
              <span>（会员昵称，主要用于显示）</span>
              <input class="form-control" name="nickname" placeholder="请输入昵称" value="<?php echo $info['nickname']; ?>" type="text">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>修改密码</label>
              <span>（修改密码-不修改为空即可）</span>
              <input class="form-control" name="password" placeholder="请输入密码" type="password">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>邮箱</label>
              <span>（用户邮箱，可为空）</span>
              <input class="form-control" name="email" placeholder="请输入邮箱" value="<?php echo $info['email']; ?>" type="text">
            </div>
          </div>
 		
 		  
 		  <div class="col-md-6">
            <div class="form-group">
              <label>微信</label>
              <span>(用户微信, 可为空)</span>
              
              <input class="form-control" name="wechat" placeholder="请输入微信" type="text" value="<?php echo $info['wechat']; ?>">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>QQ</label>
              <span>(用户qq, 可为空)</span>
              
              <input class="form-control" name="qq" placeholder="请输入QQ" type="text" value="<?php echo $info['qq']; ?>">
            </div>
          </div>
 		  
 		  
 		  
 		  
          <div class="col-md-6">
            <div class="form-group">
              <label>手机</label>
              <span>（手机号码，用于找回密码等安全操作）</span>
              <input class="form-control" name="mobile" placeholder="请输入手机号码" value="<?php echo $info['mobile']; ?>" type="text">
            </div>
          </div>
          <?php if($access['group_id'] == '19'): ?>
          <div class="col-md-6">
            <div class="form-group">
              <label>会员等级</label>
              <span></span>
              
              <select name="rank" class="form-control">
              	<option value="1" <?php if($info['rank'] == '1'): ?> selected="selected" <?php endif; ?>>普通会员</option>
              	<option value="2" <?php if($info['rank'] == '2'): ?> selected="selected" <?php endif; ?>>中级会员</option>
              	<option value="3" <?php if($info['rank'] == '3'): ?> selected="selected" <?php endif; ?>>高级会员</option>
              </select>
            </div>
          </div>
			<?php endif; ?>
          <div class="col-md-6">
            <div class="form-group">
              <label>备注信息</label>
              <span>（备注信息，可为空）</span>
              <textarea class="form-control" name="beizhu" rows="3" placeholder="请输入备注信息"><?php echo $info['beizhu']; ?></textarea>
            </div>
          </div>


        </div>
      </div>
      <div class="box-footer">
        
        <input type="hidden" name="id" value="<?php echo tp_encrypt($info['id']); ?>"/>
          
        <button  type="submit" class="btn ladda-button ajax-post" data-style="slide-up" target-form="form_single">
    <span class="ladda-label"><i class="fa fa-send"></i> 确 定</span>
</button>

<a class="btn" onclick="javascript:history.back(-1);return false;"><i class="fa fa-history"></i> 返 回</a>
        
      </div>
    </div>
</form>
