<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\discount\discountlist.html";i:1565084047;}*/ ?>
<div class="box">
    
  <div class="box-header">

    <div class="row">
        <div class="col-sm-5">
            <ob_link><a class="btn" href="<?php echo url('discount/member'); ?>"><i class="fa fa-plus"></i> 新增会员折扣</a></ob_link>
          
        </div>
        
        <div class="col-sm-7">
            <div class="box-tools search-form pull-right">
                <div class="input-group input-group-sm">
                    
                    <input type="text" name="search_data" style="width: 200px;" class="form-control pull-right" value="<?php echo input('search_data'); ?>" placeholder="支持昵称|用户名|邮箱|手机">

                    <div class="input-group-btn">
                      <button type="button" id="search"  url="<?php echo url('memberlist'); ?>" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                    </div>

                </div>
           </div>
        </div>
    </div>
    
  </div>
    
    
  <div class="box-body table-responsive">
    <table  class="table table-bordered table-hover">
      <thead>
      <tr>
       
          <th>用户名</th>
          <th>级别</th>
          <th>比例</th>
          <th>创建时间</th>
          <th>操作</th>
      </tr>
      </thead>
      <?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?>
        <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                  <td>
                  	<?php echo $vo['nickname']; ?>
                  	
                  </td>
                  <td>
                  	
                  	<?php if($vo['rank_id'] == '1'): ?>
                  		<span class="label" style="background-color: red;">普通会员</span>
                  	<?php elseif($vo['rank_id'] == '2'): ?>
                  		<span class="label" style="background-color: orange;">中级会员</span>
                  	<?php elseif($vo['rank_id'] == '3'): ?>
                  		<span class="label" style="background-color: #FE7C7C;">高级会员</span>
                  	<?php elseif($vo['rank_id'] == '4'): ?>
                  		<span class="label" style="background-color: green;">黄金代理</span>
                  	<?php elseif($vo['rank_id'] == '5'): ?>
                  		<span class="label" style="background-color: blue;">铂金代理</span>
                  	<?php else: ?>
                  		<span class="label" style="background-color: #8C08F1;">钻石代理</span>
                  	<?php endif; ?>
                  </td>
                  <td><?php echo $vo['discount']; ?>%</td>
                  <td><?php echo $vo['create_time']; ?></td>
              
                  <td class="col-md-3 text-center">
                  	<ob_link><a href="<?php echo url('discountedit', array('id' => tp_encrypt($vo['id']))); ?>" class="btn"><i class="fa fa-edit"></i> 编 辑</a></ob_link>
                  	&nbsp;&nbsp;
                		<ob_link><a class="btn confirm ajax-get"  href="<?php echo url('discountdel',array('id' => tp_encrypt($vo['id']))); ?>"><i class="fa fa-trash-o"></i> 删 除</a></ob_link>
                  </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
        <?php else: ?>
        <tbody><tr class="odd"><td colspan="8" class="text-center" valign="top"><?php echo config('empty_list_describe'); ?></td></tr></tbody>
      <?php endif; ?>
      
    </table>
  </div>

  <div class="box-footer clearfix text-center">
		<?php echo $list->render(); ?>
  </div>

</div>

<script>
    //导出功能
    $(".export").click(function(){
        
        window.location.href = searchFormUrl($(".export"));
    });
</script>