<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:92:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\member\member_yh_list.html";i:1567042955;}*/ ?>
<div class="box">
    
  <div class="box-header">

    <div class="row">
        <div class="col-sm-5">
            <ob_link><a class="btn" href="<?php echo url('memberAddyh'); ?>"><i class="fa fa-plus"></i> 新增用户</a></ob_link>
            &nbsp;
            <ob_link><a class="btn export" url="<?php echo url('exportMemberYhList'); ?>"><i class="fa fa-download"></i> 导 出</a></ob_link>
        </div>
        
        <div class="col-sm-7">
            <div class="box-tools search-form pull-right">
                <div class="input-group input-group-sm">
                    
                    <input type="text" name="search_data" style="width: 200px;" class="form-control pull-right" value="<?php echo input('search_data'); ?>" placeholder="支持昵称|用户名|邮箱|手机">

                    <div class="input-group-btn">
                      <button type="button" id="search"  url="<?php echo url('memberyhList'); ?>" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                    </div>

                </div>
           </div>
        </div>
    </div>
    
  </div>
    
    
  <div class="box-body table-responsive">
    <table  class="table table-bordered table-hover">
      <thead>
      <tr>
          <th>昵称<a href="<?php echo url('memberyhList',array('id'=>'desc')); ?>"><i class="fa fa-fw fa-sort"></i></a></th>
          <th>用户名</th>
          <!--<th>邮箱</th>-->
          <th>网站数量</th>
          <th>手机</th>
          <th>注册时间</th>
          <th>账户余额<a href="<?php echo url('memberyhList',array('sort'=>'asc')); ?>"><i class="fa fa-fw fa-sort"></i></a></th>
          <th>优化公司</th>
          <th>备注</th>
          <th>状态</th>
          <th>操作</th>
      </tr>

      </thead>
      
      <?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?>
        <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                  <td><?php echo $vo['nickname']; ?></td>
                  <td><?php echo $vo['username']; ?></td>
                  <!--<td><?php echo (isset($vo['email']) && ($vo['email'] !== '')?$vo['email']:'未绑定'); ?></td>-->
                  <td><a href="<?php echo url('seo/weblist'); ?>?search_data=<?php echo $vo['nickname']; ?>"><?php echo $vo['webnum']; ?></a></td>
                  <td><?php echo (isset($vo['mobile']) && ($vo['mobile'] !== '')?$vo['mobile']:'未绑定'); ?></td>
                  <td><?php echo $vo['create_time']; ?></td>
                    <td><span style="<?php if($vo['price'] < '1000'): ?>color: red;<?php endif; ?>"><?php echo $vo['price']; ?></span>元</td>
                  <td><?php echo $vo['leader_nickname']; ?></td>
                    <td style="max-width: 200px;"><?php echo $vo['beizhu']; ?></td>
                  <td><?php echo $vo['status_text']; ?></td>
                  <td class="col-md-3 text-center">
                      <ob_link><a href="<?php echo url('memberEdit', array('id' => tp_encrypt($vo['id']))); ?>" class="btn"><i class="fa fa-edit"></i> 编 辑</a></ob_link>
                      &nbsp;
                      <ob_link><a href="<?php echo url('seo/chongzhi', array('id' => tp_encrypt($vo['id']))); ?>" class="btn"><i class="fa fa-money"></i> 充值</a></ob_link>
                      &nbsp;
                    <!--  <ob_link><a class="btn"  href="<?php echo url('memberAuth', array('id' => $vo['id'])); ?>"><i class="fa fa-user-plus"></i> 授 权</a></ob_link>-->
                      &nbsp;
                      <ob_link><a class="btn confirm ajax-get"  href="<?php echo url('memberDel', array('id' => tp_encrypt($vo['id']))); ?>"><i class="fa fa-trash-o"></i> 删 除</a></ob_link>
                  </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
        <?php else: ?>
        <tbody><tr class="odd"><td colspan="8" class="text-center" valign="top"><?php echo config('empty_list_describe'); ?></td></tr></tbody>
      <?php endif; ?>
    </table>
  </div>

  <div class="box-footer clearfix text-center">
      <?php echo $list->render(); ?>
  </div>

</div>

<script>
    //导出功能
    $(".export").click(function(){
        
        window.location.href = searchFormUrl($(".export"));
    });
</script>