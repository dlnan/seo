<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:86:"D:\PhpStudy\PHPTutorial\WWW\03seo7mb.net\public/../app/admin\view\seo\keywordedit.html";i:1565765232;s:92:"D:\PhpStudy\PHPTutorial\WWW\03seo7mb.net\public/../app/admin\view\layout\edit_btn_group.html";i:1562986698;}*/ ?>
<form action="<?php echo url(); ?>" method="post" class="form_single">
    <div class="box">
      <div class="box-body">
        <div class="row">

          <div class="col-md-6">
            <div class="form-group">
              <label>关键词名称</label>
              <span>（关键词全称）</span>
              <input class="form-control" name="keywords"  value="<?php echo $info['keywords']; ?>" <?php if($member['leader_id'] >= 1): ?> readonly <?php endif; ?>  type="text">
            </div>
          </div>
 
          <div class="col-md-6">
            <div class="form-group">
              <label>第一页价格</label>
              <input class="form-control" name="page_one"  value="<?php echo $info['page_one']; ?>" <?php if($member['leader_id'] >= 1): ?> readonly <?php endif; ?>  placeholder="请输入第一页价格" type="number">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>第二页价格</label>
              <input class="form-control" name="page_tow"  value="<?php echo $info['page_tow']; ?>" <?php if($member['leader_id'] >= 1): ?> readonly <?php endif; ?>  placeholder="请输入第二页价格" type="number">
            </div>
          </div>

          <?php if($member['leader_id'] == 0): ?>
            <div class="col-md-6" <?php if($member['leader_id'] >= 1): ?> style="display: none" <?php endif; ?>>
              <div class="form-group">
                <label>第一页成本价</label>
                <input class="form-control" name="page_one_cb"  value="<?php echo $info['page_one_cb']; ?>" <?php if($member['leader_id'] >= 1): ?> readonly <?php endif; ?>  placeholder="请输入第一页成本价" type="number">
              </div>
            </div>

            <div class="col-md-6" >
              <div class="form-group">
                <label>第二页成本价</label>
                <input class="form-control" name="page_tow_cb"  value="<?php echo $info['page_tow_cb']; ?>" <?php if($member['leader_id'] >= 1): ?> readonly <?php endif; ?>  placeholder="请输入第二页成本" type="number">
              </div>
            </div>
          <?php endif; ?>
 			<div class="col-md-6" >
              <div class="form-group">
                <label>审核不通过原因</label>
                <span>(如果该关键词审核不通过,请填写审核不通过原因)</span>
                <input class="form-control" name="page_tow_cb"  value="<?php echo $info['beizhu']; ?>"  placeholder="请填写审核不通过原因" type="text">
              </div>
            </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>选择搜索引擎</label>
              <div>
              <label class="radio-inline">
                <input type="radio" name="search_engine" <?php if($info['search_engine'] == 'baidupc'): ?> checked="checked" <?php endif; ?> id="inlineRadio1" value="baidupc"> 百度PC
              </label>
              <label class="radio-inline">
                <input type="radio" name="search_engine" <?php if($info['search_engine'] == 'baidumobile'): ?> checked="checked" <?php endif; ?> id="inlineRadio2" value="baidumobile"> 百度移动端
              </label>
              <label class="radio-inline">
                <input type="radio" name="search_engine" <?php if($info['search_engine'] == '360pc'): ?> checked="checked" <?php endif; ?> id="inlineRadio3" value="360pc"> 360PC
              </label>

                <label class="radio-inline">
                  <input type="radio" name="search_engine" <?php if($info['search_engine'] == '360mobile'): ?> checked="checked" <?php endif; ?> id="inlineRadio4" value="360mobile"> 360移动端
                </label>

                <label class="radio-inline">
                  <input type="radio" name="search_engine" <?php if($info['search_engine'] == 'sogoupc'): ?> checked="checked" <?php endif; ?> id="inlineRadio5" value="sogoupc"> 搜狗PC
                </label>

                <label class="radio-inline">
                  <input type="radio" name="search_engine" <?php if($info['search_engine'] == 'sogoumobile'): ?> checked="checked" <?php endif; ?> id="inlineRadio6" value="sogoumobile"> 搜狗移动端
                </label>

                <label class="radio-inline">
                  <input type="radio" name="search_engine" <?php if($info['search_engine'] == 'smmobile'): ?> checked="checked" <?php endif; ?> id="inlineRadio7" value="smmobile"> 神马搜索
                </label>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>审核状态</label>
              <div>

                <label class="radio-inline">
                  <input type="radio" name="jk_status" <?php if($info['jk_status'] == '1'): ?> checked="checked" <?php endif; ?>  value="1"> 审核通过
                </label>

                <label class="radio-inline">
                  <input type="radio" name="jk_status" <?php if($info['jk_status'] == '0'): ?> checked="checked" <?php endif; ?>  value="0"> 待管理员审核
                </label>
                <label class="radio-inline">
                  <input type="radio" name="jk_status" <?php if($info['jk_status'] == '-1'): ?> checked="checked" <?php endif; ?>  value="-1"> 待代理审核
                </label>
                <label class="radio-inline">
                  <input type="radio" name="jk_status" <?php if($info['jk_status'] == '-2'): ?> checked="checked" <?php endif; ?>  value="-2"> 审核不通过
                </label>
             <span style="color: red;">注意:只有审核通过才会监控关键词</span>
              </div>
            </div>
          </div>
			<div class="col-md-6">
            <div class="form-group">
              <label>优化状态</label>
              <div>
				<label class="radio-inline">
                  <input type="radio" name="youhua" <?php if($info['youhua'] == '2'): ?> checked="checked" <?php endif; ?>  value="2"> 未优化
                </label>
                <label class="radio-inline">
                  <input type="radio" name="youhua" <?php if($info['youhua'] == '1'): ?> checked="checked" <?php endif; ?>  value="1"> 优化中
                </label>

                <label class="radio-inline">
                  <input type="radio" name="youhua" <?php if($info['youhua'] == '0'): ?> checked="checked" <?php endif; ?>  value="0"> 待代理审核停止优化
                </label>
                <label class="radio-inline">
                  <input type="radio" name="youhua" <?php if($info['youhua'] == '-1'): ?> checked="checked" <?php endif; ?>  value="-1"> 待管理员审核停止优化
                </label>
                <label class="radio-inline">
                  <input type="radio" name="youhua" <?php if($info['youhua'] == '-2'): ?> checked="checked" <?php endif; ?>  value="-2"> 停止优化
                </label>
             <span style="color: red;">注意:只有优化中才会监控关键词</span>
              </div>
            </div>
          </div>
          <input type="hidden" name="id" value="<?php echo tp_encrypt($info['id']); ?>">
            
        </div>
      </div>
      <div class="box-footer">
        
        <button  type="submit" class="btn ladda-button ajax-post" data-style="slide-up" target-form="form_single">
    <span class="ladda-label"><i class="fa fa-send"></i> 确 定</span>
</button>

<a class="btn" onclick="javascript:history.back(-1);return false;"><i class="fa fa-history"></i> 返 回</a>
      </div>
    </div>
</form>


