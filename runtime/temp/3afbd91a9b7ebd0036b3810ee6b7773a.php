<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:85:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\seo\zhishuedit.html";i:1562986698;s:92:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\layout\edit_btn_group.html";i:1562986698;}*/ ?>
<form action="<?php echo url(); ?>" method="post" class="form_single">
    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>第一页价格</label>
              <input class="form-control" name="page_one" placeholder="请输入第一页价格" value="<?php echo $list['page_one']; ?>" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>第二页价格</label>
              <input class="form-control" name="page_two" placeholder="请输入第二页价格" value="<?php echo $list['page_two']; ?>" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>关键词指数范围</label>
              <input class="form-control" name="zhisu_range" placeholder="请输入关键词指数范围" value="<?php echo $list['zhisu_range']; ?>" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>搜索引擎</label>
              
              <select name="search_engin" class="form-control">
              	  <option value="百度" <?php if($list['search_engin'] == '百度'): ?>selected="selected"<?php else: endif; ?>>百度</option>
              	  <option value="百度移动" <?php if($list['search_engin'] == '百度移动'): ?>selected="selected"<?php else: endif; ?>>百度移动</option>
              	  <option value="360" <?php if($list['search_engin'] == '360'): ?>selected="selected"<?php else: endif; ?>>360</option>
              	  <option value="360移动" <?php if($list['search_engin'] == '360移动'): ?>selected="selected"<?php else: endif; ?>>360移动</option>
              	  <option value="搜狗" <?php if($list['search_engin'] == '搜狗'): ?>selected="selected"<?php else: endif; ?>>搜狗</option>
              	  <option value="搜狗移动" <?php if($list['search_engin'] == '搜狗移动'): ?>selected="selected"<?php else: endif; ?>>搜狗移动</option>
              	  <option value="神马" <?php if($list['search_engin'] == '神马'): ?>selected="selected"<?php else: endif; ?>>神马</option>
              </select>
            </div>
          </div>
           <div class="col-md-6">
            <div class="form-group">
              <label>第一页成本价</label>
              <input class="form-control" name="chenben_one" placeholder="请输入第一页成本价" value="<?php echo $list['chenben_one']; ?>" type="text">
            </div>
          </div>
           <div class="col-md-6">
            <div class="form-group">
              <label>第二页成本价</label>
              <input class="form-control" name="chenben_two" placeholder="请输入第二页成本价" value="<?php echo $list['chenben_two']; ?>" type="text">
            </div>
          </div>
    
        </div>
      </div>
      <div class="box-footer">
        
        <input type="hidden" value="<?php echo $list['id']; ?>" name='id' />
          
        <button  type="submit" class="btn ladda-button ajax-post" data-style="slide-up" target-form="form_single">
    <span class="ladda-label"><i class="fa fa-send"></i> 确 定</span>
</button>

<a class="btn" onclick="javascript:history.back(-1);return false;"><i class="fa fa-history"></i> 返 回</a>
        
      </div>
    </div>
</form>
