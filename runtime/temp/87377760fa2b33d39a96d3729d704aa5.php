<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:91:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\member\member_yh_add.html";i:1566441470;s:92:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\layout\edit_btn_group.html";i:1562986698;}*/ ?>
<form action="<?php echo url(); ?>" method="post" class="form_single">
    <div class="box">
      <div class="box-body">
        <div class="row">

          <div class="col-md-6">
            <div class="form-group">
              <label>所属代理用户：</label>
              <span>（所属代理用户）</span>


             <?php if($user['leader_id'] == '0'): ?>

              <select class="form-control" name="dl">
                <option value="0">请选择所属代理</option>

                <?php if(is_array($info) || $info instanceof \think\Collection || $info instanceof \think\Paginator): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?>
                <option value="<?php echo $data['id']; ?>"><?php echo $data['nickname']; ?></option>
               <?php endforeach; endif; else: echo "" ;endif; ?>
              </select>
              <?php else: ?>
              <select class="form-control" name="dl">
                <option value="<?php echo $user['id']; ?>"><?php echo $user['nickname']; ?></option>
              </select>
              <?php endif; ?>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>用户名</label>
              <span>（用户名会作为默认的昵称）</span>
              <input class="form-control" name="username" placeholder="请输入用户名" type="text">
            </div>
          </div>
 
          <div class="col-md-6">
            <div class="form-group">
              <label>密码</label>
              <span>（用户密码不能少于6位）</span>
              <input class="form-control" name="password" placeholder="请输入密码" type="password">
            </div>
          </div>
 
          <div class="col-md-6">
            <div class="form-group">
              <label>确认密码</label>
              <span>（确认密码需要和密码一致）</span>
              <input class="form-control" name="password_confirm" placeholder="请输入确认密码" type="password">
            </div>
          </div>
 			
 		  <div class="col-md-6">
            <div class="form-group">
              <label>会员等级</label>
              <span></span>
              
              <select name="rank" class="form-control">
              	<option value="1" selected="selected">普通会员</option>
              	<option value="2">中级会员</option>
              	<option value="3">高级会员</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>微信</label>
              <span>(用户微信, 可为空)</span>
              
              <input class="form-control" name="wechat" placeholder="请输入微信" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>QQ</label>
              <span>(用户qq, 可为空)</span>
              
              <input class="form-control" name="qq" placeholder="请输入QQ" type="text">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>邮箱</label>
              <span>（用户邮箱，可以为空）</span>
              <input class="form-control" name="email" placeholder="请输入邮箱" type="text">
            </div>
          </div>


            <div class="col-md-6">
                <div class="form-group">
                    <label>手机</label>
                    <span>（手机号码，用于找回密码等安全操作）</span>
                    <input class="form-control" name="mobile" placeholder="请输入手机号码" value="" type="text">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>备注信息</label>
                    <span>（备注信息，可为空）</span>
                    <textarea class="form-control" name="beizhu" rows="3" placeholder="请输入备注信息"></textarea>
                </div>
            </div>
            

            
        </div>
      </div>
      <div class="box-footer">
        
        <button  type="submit" class="btn ladda-button ajax-post" data-style="slide-up" target-form="form_single">
    <span class="ladda-label"><i class="fa fa-send"></i> 确 定</span>
</button>

<a class="btn" onclick="javascript:history.back(-1);return false;"><i class="fa fa-history"></i> 返 回</a>
      </div>
    </div>
</form>
