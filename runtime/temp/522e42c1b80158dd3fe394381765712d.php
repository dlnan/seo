<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:89:"D:\PhpStudy\PHPTutorial\WWW\05seo7mb.net\public/../app/admin\view\seo\keywordloglist.html";i:1569489811;}*/ ?>
<div class="box">
    
  <div class="box-header">

    <div class="row">
        <div class="col-sm-5">

        </div>
        
        <div class="col-sm-7">
            <div class="box-tools search-form pull-right">
                <div class="input-group input-group-sm">

                    <select name="search_engine" class="form-control pull-right" style="width: 200px;" >
                        <option value="">请选择搜索引擎</option>
                        <option value="baidupc" <?php if(input('search_engine') == 'baidupc'): ?> selected = "selected" <?php endif; ?> >百度</option>
                        <option value="baidumobile" <?php if(input('search_engine') == 'baidumobile'): ?> selected = "selected" <?php endif; ?> >百度移动端</option>
                        <option value="360pc" <?php if(input('search_engine') == '360pc'): ?> selected = "selected" <?php endif; ?> >360</option>
                        <option value="360mobile" <?php if(input('search_engine') == '360mobile'): ?> selected = "selected" <?php endif; ?> >360移动端</option>
                        <option value="sougoupc" <?php if(input('search_engine') == 'sogoupc'): ?> selected = "selected" <?php endif; ?> >搜狗</option>
                        <option value="sougoumobile" <?php if(input('search_engine') == 'sogoumobile'): ?> selected = "selected" <?php endif; ?> >搜狗移动端</option>
                        <option value="smmobile" <?php if(input('search_engine') == 'smmobile'): ?> selected = "selected" <?php endif; ?> >神马搜索</option>

                    </select>

                    <input type="text" name="search_username" style="width: 200px;" class="form-control pull-right" value="<?php echo input('search_username'); ?>" placeholder="输入用户名">

                    <input type="text" name="search_keyword" style="width: 200px;" class="form-control pull-right" value="<?php echo urldecode(input('search_keyword')); ?>" placeholder="输入网站关键词搜索">

                    <input type="text" name="search_data" style="width: 200px;" class="form-control pull-right" value="<?php echo input('search_data'); ?>" placeholder="输入网站域名搜索">

                    <div class="input-group-btn">
                      <button type="button" id="search"  url="<?php echo url('keywordlistlog'); ?>" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                    </div>

                </div>
           </div>
        </div>
    </div>
    
  </div>
    
    
  <div class="box-body table-responsive">
    <table  class="table table-bordered table-hover">
      <thead>
      <tr>
          <th>关键词</th>
          <th>所属域名</th>
          <th>搜索引擎</th>
          <th>所属用户</th>
          <th>查询时间</th>
          <th>排名</th>
          <th>节点名称</th>

          <th>排名截图</th>
      </tr>
      </thead>
      
      <?php if(!(empty($list["0"]["id"]) || (($list["0"]["id"] instanceof \think\Collection || $list["0"]["id"] instanceof \think\Paginator ) && $list["0"]["id"]->isEmpty()))): ?>
        <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                  <td>

                     <?php switch($vo['search_engine']): case "baidupc": ?>
                      <a class="" href="https://www.baidu.com/s?ie=UTF-8&amp;wd=<?php echo $vo['keywords']; ?>" target="_blank"><?php echo $vo['keywords']; ?></a>
                      <?php break; case "360pc": ?>
                      <a class="" href="http://www.sogou.com/web?query=<?php echo $vo['keywords']; ?>" target="_blank"><?php echo $vo['keywords']; ?></a>
                      <?php break; case "sougoupc": ?>
                      <a class="" href="https://www.so.com/s?ie=utf-8&amp;fr=none&amp;src=360sou_newhome&amp;q=<?php echo $vo['keywords']; ?>" target="_blank"><?php echo $vo['keywords']; ?></a>
                      <?php break; default: ?>
                      <?php echo $vo['keywords']; endswitch; ?>

                  </td>
                  <td><?php echo $vo['url']; ?></td>


                    <td>
                        <?php if($vo['search_engine'] == 'baidumobile'): ?> <span class="label" style="background-color: #ef5350"> 百度移动端</span>  <?php endif; if($vo['search_engine'] == 'baidupc'): ?> <span class="label" style="background-color: #33ca35"> 百度</span> <?php endif; if($vo['search_engine'] == '360pc'): ?><span class="label" style="background-color: #4484ca"> 360</span> <?php endif; if($vo['search_engine'] == '360mobile'): ?> <span class="label" style="background-color: #f72f8e"> 360手机端</span><?php endif; if($vo['search_engine'] == 'sogoupc'): ?> <span class="label" style="background-color: #bf8fca"> 搜狗</span><?php endif; if($vo['search_engine'] == 'sogoumobile'): ?> <span class="label" style="background-color: #66b5ca"> 搜狗移动端</span><?php endif; if($vo['search_engine'] == 'smmobile'): ?> <span class="label" style="background-color: #f732bc"> 神马搜索</span><?php endif; ?>

                    </td>

                    <td><?php echo $vo['username']; ?></td>

                    <td><?php echo format_time($vo['time']); ?></td>

                    <td>
                        <?php if($vo['time'] == ''): ?>
                         查询中....
                        <?php else: if($vo['Rank'] == '0'): ?>更新时间<span style="color: #4c3fff;font-size: 18px;">【<?php echo format_time($vo['time']); ?>】</span>,排名: <span style="color: #4c3fff;font-size: 18px;">20+</span> <?php else: ?>更新时间<span style="color: #4c3fff;font-size: 18px;">【<?php echo format_time($vo['time']); ?>】</span>,排名: <span style="color: red;font-size: 18px;"><?php echo $vo['Rank']; ?></span> <?php endif; endif; ?>
                    </td>
                  <td>
                      <span class="label label-success"><?php echo $vo['area']; ?></span>
                  </td>

                    <td>
                        <?php if($vo['img_url'] == ''): ?>
                        暂时没有截图
                        <?php else: ?>
                        <span class="label">
                        	
                        	<a href="javascript:;" onclick="window.open('<?php echo $vo['img_url']; ?>','','height=600,width=700,top=250,left=700')">查看截图</a>
                        </span>
                        <?php endif; ?>
                    </td>

                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
        <?php else: ?>
        <tbody><tr class="odd"><td colspan="8" class="text-center" valign="top"><?php echo config('empty_list_describe'); ?></td></tr></tbody>
      <?php endif; ?>
    </table>
  </div>

  <div class="box-footer clearfix text-center">
      <?php echo $list->render(); ?>
  </div>

</div>
<div id="tanchuimage1">
        <div id="tanchuimage">

        </div>
</div>
<script>
    //导出功能
    $(".export").click(function(){
        
        window.location.href = searchFormUrl($(".export"));
    });
            //弹出图片功能
        function ejectimg(num){
            img = $("#img" + num).attr('data-img');
            if(img != ''){
                $("#tanchuimage1").css('display','block');
                $("#tanchuimage").css('display','block');
                $("#tanchuimage").html("<img src='" + img + "'>");
            }else{
                toast.warning('当前关键词没有截图!');
            }


            $("#tanchuimage1").click(function(){


                    $("#tanchuimage").css('display','none');
                    $("#tanchuimage1").css('display','none');


            });


        };
</script>
<style type="text/css">
	#tanchuimage1{display: none;position: absolute;width: 100%;height: 93%;background-color: rgb(33,33,33,0.7);top:6%;z-index: 9}
        #tanchuimage{max-width:100%;z-index: 9;max-height: 100%;position: absolute;display: none;top:12%;background-color: rgb(33,33,33,0.7);height: 700px;overflow-y: scroll;left: 16%;}

        #tanchuimage img{ left: 34%;   width: 100%;}
</style>