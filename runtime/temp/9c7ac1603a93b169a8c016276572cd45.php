<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:87:"D:\PhpStudy\PHPTutorial\WWW\03seo7mb.net\public/../app/admin\view\seo\chongzhilist.html";i:1565769272;}*/ ?>
<div class="box">
    
  <div class="box-header">

    <div class="row">
        <div class="col-sm-10">
            <!--
            <ob_link><a class="btn" href="<?php echo url('webAdd'); ?>"><i class="fa fa-plus"></i> 新增网站</a></ob_link>
            -->
        </div>
        
        <div class="col-sm-2">
            <div class="box-tools search-form pull-right">

                
                <div class="input-group input-group-sm">
                    
                    
                    <select name="pay_status" class="form-control pull-right">
                    	<option value="0">未支付</option>
                    	<option value="1">已支付</option>
                    </select>

                    <div class="input-group-btn">
                      <button type="button" id="search"  url="<?php echo url('chongzhilist'); ?>" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
                    </div>

                </div>

                
           </div>
        </div>
    </div>
    
  </div>
    

  <div class="box-body table-responsive">
    <table  class="table table-bordered table-hover">
      <thead>
      <tr>
          <th>用户名</th>
          <th>金额</th>
          <th>备注</th>
          <th>充值时间</th>
          <th>充值公司</th>
          <th>订单号</th>
          <th>是否支付</th>
          <th <?php if($member['id'] == 1): ?> style="display: block;" <?php else: ?> style="display: none;" <?php endif; ?>>操作</th>

      </tr>
      </thead>
      
      <?php if(!(empty($list["0"]["id"]) || (($list["0"]["id"] instanceof \think\Collection || $list["0"]["id"] instanceof \think\Paginator ) && $list["0"]["id"]->isEmpty()))): ?>
        <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <tr>
                  <td><?php echo $vo['username']; ?></td>
                  <td align="center" style="color: #ef5350;">+<?php echo $vo['price']; ?></td>
                  <td><?php echo (isset($vo['beizhu']) && ($vo['beizhu'] !== '')?$vo['beizhu']:'没有备注'); ?></td>
                  <td><?php echo $vo['create_time']; ?></td>
                  <td>
                      <?php if($vo['gs'] == ''): ?>
                        <?php echo $vo['beizhu']; else: ?>
                        <?php echo $vo['gs']; endif; ?>
                  </td>
                    <td><?php echo $vo['pay_order']; ?></td>
                    <td><?php if($vo['pay_status'] == 0): ?>
                            <span style="background-color: red;color:#ffffff;padding: 6px;">未支付</span>
                        <?php else: ?>
                            <span style="background-color: #00A65A;color:#ffffff;padding: 6px;">已支付</span>
                        <?php endif; ?>
                    </td>
                   <td <?php if($member['id'] == 1): ?> style="display: block;" <?php else: ?> style="display: none;" <?php endif; ?>>
                     <ob_link><a href="<?php echo url('chongzhiedit', array('id' => tp_encrypt($vo['id']))); ?>" class="btn"><i class="fa fa-edit"></i> 编 辑</a></ob_link>
                     &nbsp;&nbsp;
                     <ob_link><a class="btn confirm ajax-get" href="<?php echo url('delchongzhi',array('id' => tp_encrypt($vo['id']))); ?>"><i class="fa fa-trash-o"></i> 删 除</a></ob_link>
                   </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
        <?php else: ?>
        <tbody><tr class="odd"><td colspan="8" class="text-center" valign="top"><?php echo config('empty_list_describe'); ?></td></tr></tbody>
      <?php endif; ?>
    </table>
  </div>

  <div class="box-footer clearfix text-center">
      <?php echo $list->render(); ?>
  </div>

</div>

<script>
    //导出功能
    $(".export").click(function(){
        
        window.location.href = searchFormUrl($(".export"));
    });
</script>